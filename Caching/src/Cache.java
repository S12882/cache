
import java.util.*;


public class Cache  {
	

   private static int timeToLive; 
   private static Cache instance;
   private static HashMap<String, EnumValue> items  = new HashMap<>();
 
   
   private Cache(){	   
   }
   
   public static  Cache getInstance(){ 
	   if(instance == null){
		   synchronized(Cache.class){
			   if(instance == null){
				   instance = new Cache();
				   timeToLive = 100000;
			   }
		   }
	   }
	   return instance;
   }
   
   public static void loadItems(){
	   if (items == null){
		   try{
			   items = new HashMap<String, EnumValue>();
		   } catch(Exception e){
			   items = null;
			  e.getStackTrace();
		   }
	   }
   }
   

   public void clean(){ 
	   try { 
		     {
		    	 Random r = new Random();
		    	 items.clear();
		    	 Thread.sleep(timeToLive*(int)r.nextDouble());
		     }
	   }catch (InterruptedException e){
		   e.printStackTrace();
	   }
	    
   }
   
   public static void update(){
	   	
   }

   
}
